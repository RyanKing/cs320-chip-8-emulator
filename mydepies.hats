(*
#
# HX:
# Handling dependency for
# the chip-8-emulator project
#
*)
(* ****** ****** *)
//
#define
LIBSDL2_targetloc
"$PATSHOMELOCS/atscntrb-libsdl2"
//
(* ****** ****** *)
//
staload SDL2 = "{$LIBSDL2}/SATS/SDL.sats"

//
(* ****** ****** *)

(* end of [mydepies.hats] *)
