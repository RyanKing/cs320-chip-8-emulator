(* ****** ****** *)
//
// HX-2017-02-09:
// This is a small project
// So I put all of the declarations
// inside this single file
//
(* ****** ****** *)

(*
//
// HX-2017-02-09:
// In general, one should try to avoid
// dependent types at the very beginning!
//
*)
(* ****** ****** *)

%{#
#include "../CATS/chip-8.cats"
%}

#define ATS_EXTERN_PREFIX "chip8_"

(* ****** ****** *)

staload "./../mydepies.hats"

(* ****** ****** *)

abst@ype byte = uint8
abst@ype word = uint16

(* ****** ****** *)

castfn chip8_byte_of_int(int):<> byte
castfn chip8_int_of_byte(byte):<> int
castfn chip8_byte_of_char(char):<> byte
castfn chip8_word_of_int(int):<> word
castfn chip8_int_of_word(word):<> int
castfn chip8_byte_of_word(word):<> byte
castfn chip8_word_of_byte(byte):<> word

symintr i2b b2i c2b i2w w2i b2w w2b
overload i2b with chip8_byte_of_int
overload b2i with chip8_int_of_byte
overload c2b with chip8_byte_of_char
overload i2w with chip8_word_of_int
overload w2i with chip8_int_of_word
overload b2w with chip8_word_of_byte
overload w2b with chip8_byte_of_word

(* ****** ****** *)

macdef b_0x0 = $extval(byte, "0x0")
macdef b_0x1 = $extval(byte, "0x1")

macdef w_0x0   = $extval(word, "0x0")
macdef w_0x1   = $extval(word, "0x1")
macdef w_0x2   = $extval(word, "0x2")
macdef w_0x3   = $extval(word, "0x3")
macdef w_0x4   = $extval(word, "0x4")
macdef w_0x5   = $extval(word, "0x5")
macdef w_0x6   = $extval(word, "0x6")
macdef w_0x7   = $extval(word, "0x7")
macdef w_0x8   = $extval(word, "0x8")
macdef w_0x9   = $extval(word, "0x9")
macdef w_0xA   = $extval(word, "0xA")
macdef w_0xB   = $extval(word, "0xB")
macdef w_0xC   = $extval(word, "0xC")
macdef w_0xD   = $extval(word, "0xD")
macdef w_0xE   = $extval(word, "0xE")
macdef w_0xF   = $extval(word, "0xF")
macdef w_0xFF  = $extval(word, "0xFF")
macdef w_0xFFF = $extval(word, "0xFFF")

(* ****** ****** *)

fun eq_byte_byte(byte, byte): bool = "mac#%"
fun neq_byte_byte(byte, byte): bool = "mac#%"
fun gt_byte_byte(byte, byte): bool = "mac#%"
fun lt_byte_byte(byte, byte): bool = "mac#%"
fun add_byte_byte(byte, byte): byte = "mac#%"
fun sub_byte_byte(byte, byte): byte = "mac#%"
fun mult_byte_byte(byte, byte): byte = "mac#%"
fun div_byte_byte(byte, byte): byte = "mac#%"
fun mod_byte_byte(byte, byte): byte = "mac#%"
fun land_byte_byte(byte, byte): byte = "mac#%"
fun lor_byte_byte(byte, byte): byte = "mac#%"
fun lxor_byte_byte(byte, byte): byte = "mac#%"
fun lsl_byte(byte, int): byte = "mac#%"
fun lsr_byte(byte, int): byte = "mac#%"

fun eq_word_word(word, word): bool = "mac#%"
fun add_word_word(word, word): word = "mac#%"
fun sub_word_word(word, word): word = "mac#%"
fun land_word_word(word, word): word = "mac#%"
fun lsr_word(word, int): word = "mac#%"

fun eq_byte_word(byte, word): bool = "mac#%"
fun neq_byte_word(byte, word): bool = "mac$%"

overload = with eq_byte_byte
overload != with neq_byte_byte
overload > with gt_byte_byte
overload < with lt_byte_byte
overload + with add_byte_byte
overload - with sub_byte_byte
overload * with mult_byte_byte
overload / with div_byte_byte
overload % with mod_byte_byte
overload land with land_byte_byte
overload lor with lor_byte_byte
overload lxor with lxor_byte_byte
overload lsl with lsl_byte
overload lsr with lsr_byte

overload = with eq_word_word
overload + with add_word_word
overload - with sub_word_word
overload land with land_word_word
overload lsr with lsr_word

overload = with eq_byte_word
overload != with neq_byte_word

fun succ_byte : byte -> byte
fun succ_word : word -> word

overload succ with succ_byte
overload succ with succ_word

fun word_make_byte_byte(byte(*high*), byte(*low*)): word = "mac#%"

symintr to_word
overload to_word with word_make_byte_byte

(* ****** ****** *)

fun print_byte : print_type(byte)
fun print_word : print_type(word)
fun fprint_byte : fprint_type(byte)
fun fprint_word : fprint_type(word)

overload print with print_byte
overload print with print_word
overload fprint with fprint_byte
overload fprint with fprint_word

(* ****** ****** *)

#define MEM_SIZE 4096

typedef imem = natLt(MEM_SIZE)

abstype memory_type = ptr
typedef memory = memory_type

fun chip8_imem_of_int(int): imem

symintr i2a
overload i2a with chip8_imem_of_int

fun memory_get_at(imem): byte
fun memory_set_at(imem, byte): void

overload Mem with memory_get_at
overload Mem with memory_set_at

fun memory_get_byte_exn(): byte
fun memory_get_word_exn(): word

(* ****** ****** *)

#define NREG 16

abstype breg_type = ptr
typedef breg = breg_type

fun V(i: natLt(NREG)): breg

fun breg_get(r: breg): byte
fun breg_set(r: breg, v: byte): void

overload .get with breg_get // r.get()
overload .set with breg_set // r.set(v)

(* ****** ****** *)

abstype areg_type = ptr
typedef areg = areg_type

val I: areg
val PC: areg

fun areg_get(r: areg): imem
fun areg_set(r: areg, v: int): void
fun areg_incr(r: areg, amt: int): void

symintr .incr
overload .get with areg_get // r.get()
overload .set with areg_set // r.set(v)
overload .incr with areg_incr // r.incr

(* ****** ****** *)

abstype timer_type = ptr
typedef timer = timer_type
typedef time_t = double

val DT: timer
val ST: timer

fun timer_get(timer): byte
fun timer_set(timer, byte): void
fun timer_decr(timer, byte): void

symintr .decr
overload .get with timer_get
overload .set with timer_set
overload .decr with timer_decr

fun get_time(): time_t

(* ****** ****** *)

#define STK_SIZE 16

abstype stack_type = ptr
typedef stack = stack_type

fun stack_pop(): imem
fun stack_push(imem): void

exception StackOverflowExn of ()
exception StackUnderflowExn of ()

(* ****** ****** *)
//
// RK-2017-2-13:
//
#define SCR_WIDTH  64
#define SCR_HEIGHT 32
#define SPR_WIDTH 8

abstype screen_type = ptr
typedef screen = screen_type

fun screen_clear : ((*void*)) -> void

fun screen_get_at : (natLt(SCR_WIDTH), natLt(SCR_HEIGHT)) -> byte
fun screen_set_at : (natLt(SCR_WIDTH), natLt(SCR_HEIGHT), byte) -> void

overload Scr with screen_get_at
overload Scr with screen_set_at

(* ****** ****** *)
//
#define NUM_CHARS 80
//
macdef
the_chip8_font =
$extval(arrayref(byte, NUM_CHARS), "the_chip8_font")
//
fun load_font : ((*void*)) -> void
//
(* ****** ****** *)

#define NUM_CHARS 80

macdef chip8_font = $extval(arrayref(byte, NUM_CHARS), "chip8_font")

fun load_font : () -> void

(* ****** ****** *)

#define SCALE 8

absvtype display_type = ptr
vtypedef display = display_type

fun init_display : () -> display
fun close_display : (display) -> void
fun update_display : (!display) -> void

#define NUM_KEYS 16

typedef key_num = natLt(NUM_KEYS)
datatype key_event =
  | key_press of (key_num)
  | key_release of (key_num)
  | key_quit of ()

fun clear_keys : () -> void
fun check_key : (key_num) -> bool
fun press_key : (key_num) -> void
fun release_key : (key_num) -> void
fun poll_kb : () -> void
(* ****** ****** *)

#define PC_START 0x200
#define INSNS_PER_FRAME 14

fun quit : () -> void
fun load_game : (string) -> void
fun game_loop : (!display) -> void
fun exec_ins : (word) -> void

(* ****** ****** *)

(* end of [chip-8-main.sats] *)
