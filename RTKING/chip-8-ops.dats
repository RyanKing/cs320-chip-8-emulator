(* ****** ****** *)
//
// RK-2017-02-26:
// Implementing Opcodes
//
(* ****** ****** *)

#ifndef CHIP_8_MAIAN
#include "./../staloadall.hats"
#endif

staload STDLIB = "libats/libc/SATS/stdlib.sats"
staload UN = "prelude/SATS/unsafe.sats"

(* ****** ****** *)

implement exec_ins(o) =
  let
    exception UnknownOp of (word)
    exception InvalidRegister of (byte)

    macdef w2m(i) = $UN.cast{imem}(,(i))
    macdef set_vf(test) = (V(0xF)).set(if (,(test)) then b_0x1 else b_0x0)

    val u   = (o lsr 0xC) land i2w(0xF)
    val x = $UN.cast{natLt(NREG)}((o lsr 0x8) land i2w(0xF))
    val Vx   = V($UN.cast{natLt(NREG)}((o lsr 0x8) land i2w(0xF)))
    val Vy   = V($UN.cast{natLt(NREG)}((o lsr 0x4) land i2w(0xF)))
    val n   = (o lsr 0x0) land i2w(0xF)
    val kk  = w2b((o lsr 0x0) land i2w(0xFF))
    val nnn = w2i((o lsr 0x0) land i2w(0xFFF))
    val ()  = PC.incr(2)
  in
    ifcase
    | u = w_0x0 && kk = i2b(0xE0) => screen_clear()
    | u = w_0x0 && kk = i2b(0xEE) => PC.set(stack_pop())
    | u = w_0x1                   => PC.set(nnn)
    | u = w_0x2                   => (stack_push(PC.get()); PC.set(nnn))
    | u = w_0x3                   => if (Vx.get() = kk) then PC.incr(2)
    | u = w_0x4                   => if (Vx.get() != kk) then PC.incr(2)
    | u = w_0x5                   => if (Vx.get() = Vy.get()) then PC.incr(2)
    | u = w_0x6                   => Vx.set(kk)
    | u = w_0x7                   => Vx.set(Vx.get() + kk)
    | u = w_0x8 && n = w_0x0      => Vx.set(Vy.get())
    | u = w_0x8 && n = w_0x1      => Vx.set(Vx.get() lor Vy.get())
    | u = w_0x8 && n = w_0x2      => Vx.set(Vx.get() land Vy.get())
    | u = w_0x8 && n = w_0x3      => Vx.set(Vx.get() lxor Vy.get())
    | u = w_0x8 && n = w_0x4      => (
        set_vf(Vx.get() > (i2b(0xFF) - Vy.get()));
        Vx.set((Vx.get() + Vy.get()) land i2b(0xFF))
      )
    | u = w_0x8 && n = w_0x5      => (
        set_vf(Vx.get() > Vy.get());
        Vx.set((Vx.get() - Vy.get()) land i2b(0xFF))
      )
    | u = w_0x8 && n = w_0x6      => (
        set_vf((Vx.get() land b_0x1) = b_0x1);
        Vx.set(Vx.get() lsr 0x1)
      )
    | u = w_0x8 && n = w_0x7      => (
        set_vf(Vy.get() > Vx.get());
        Vx.set((Vy.get() - Vx.get()) land i2b(0xFF))
      )
    | u = w_0x8 && n = w_0xE      => (
        set_vf((Vx.get() lsr 0x7) = b_0x1);
        Vx.set((Vx.get() lsl 0x1) land i2b(0xFF))
      )
    | u = w_0x9 && n = w_0x0      =>
      if (Vx.get() != Vy.get()) then PC.incr(2)
    | u = w_0xA                   => I.set(nnn)
    | u = w_0xB                   => PC.set(b2i((V(0)).get()) + nnn)
    | u = w_0xC                   => Vx.set(i2b($STDLIB.rand() % 256) land kk)
    | u = w_0xD                   => draw_y(0) where {
        val () = (V(0xF)).set(b_0x0)
        val x = b2i(Vx.get())
        val y = b2i(Vy.get())
        val height = w2i(n)

        fun draw_x(row: byte, x_off: int, y_off: int):<cloref1> void =
          if x_off < SPR_WIDTH && x + x_off < SCR_WIDTH then
            let
              val _x = $UN.cast{natLt(SCR_WIDTH)}(x + x_off)
              val _y = $UN.cast{natLt(SCR_HEIGHT)}(y + y_off)
              val pix = (row lsr (SPR_WIDTH - 1 - x_off)) land b_0x1
              val () = if pix = b_0x1 then (
                if Scr(_x, _y) = b_0x1 then (
                  (V(0xF)).set(b_0x1);
                  Scr(_x, _y, b_0x0)
                ) else Scr(_x, _y, b_0x1)
              )
            in
              draw_x(row, succ(x_off), y_off)
            end

        fun draw_y(y_off: int):<cloref1> void = (
          if y_off < height && y + y_off < SCR_HEIGHT then (
            draw_x(Mem($UN.cast{imem}(I.get() + y_off)), 0, y_off);
            draw_y(succ(y_off))
          )
        )
      }
    | u = w_0xE && kk = i2b(0x9E) =>
      if check_key($UN.cast{key_num}(Vx.get())) then PC.incr(2)
    | u = w_0xE && kk = i2b(0xA1) =>
      if not (check_key($UN.cast{key_num}(Vx.get()))) then PC.incr(2)
    | u = w_0xF && kk = i2b(0x07) => Vx.set(DT.get())
    | u = w_0xF && kk = i2b(0x0A) =>
      let
        fun check_keys(i: key_num): int =
          if check_key(i) then i
          else if succ(i) < NUM_KEYS then check_keys(succ(i))
          else ~1
        val key = check_keys(0)
      in
        if key = ~1 then PC.set(PC.get() - 2)
        else Vx.set(i2b(key))
      end
    | u = w_0xF && kk = i2b(0x15) => DT.set(Vx.get())
    | u = w_0xF && kk = i2b(0x18) => ST.set(Vx.get())
    | u = w_0xF && kk = i2b(0x1E) => (
        set_vf(I.get() + b2i(Vx.get()) > 0xFFF);
        I.set(I.get() + b2i(Vx.get()))
      )
    | u = w_0xF && kk = i2b(0x29) => I.set(b2i(Vx.get()) * 5)
    | u = w_0xF && kk = i2b(0x33) => (
        Mem(I.get(), Vx.get() / i2b(100) % i2b(10));
        Mem(I.get(), Vx.get() / i2b(10) % i2b(10));
        Mem(I.get(), Vx.get() % i2b(10))
      )
    | u = w_0xF && kk = i2b(0x55) => store(0, x) where {
        fun store(i: natLt(NREG), n: natLt(NREG)): void = (
          Mem(i2a(I.get() + i), (V(i)).get());
          if succ(i) < NREG then if i < n then store(succ(i), n)
        )
      }
    | u = w_0xF && kk = i2b(0x65) => fill(0, x) where {
        fun fill(i: natLt(NREG), n: natLt(NREG)): void = (
          (V(i)).set(Mem(i2a(I.get() + i)));
          if succ(i) < NREG then if i < n then fill(succ(i), n)
        )
      }
    | _ => $raise UnknownOp(o)
  end

(* ****** ****** *)


(* End of [chip-8-ops.dats] *)