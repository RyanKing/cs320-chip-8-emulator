(* ****** ****** *)
//
// RK-2017-03-20
// Implementing Keyboard Input
//
(* ****** ****** *)

#ifndef CHIP_8_MAIN
#include "./../staloadall.hats"
#endif

(* ****** ****** *)

local
  val numkeys = i2sz(NUM_KEYS)
  val keys = arrayref_make_elt<bool>(numkeys, false)
in
  implement clear_keys() = loop(0) where {
    fun loop(i: key_num): void = (
      keys[i] := false;
      if succ(i) < NUM_KEYS then loop(succ(i))
    )
  }

  implement check_key(i) = keys[i]
  implement press_key(i) = keys[i] := true
  implement release_key(i) = keys[i] := false
end

(* End of [chip-8-key.dats] *)
