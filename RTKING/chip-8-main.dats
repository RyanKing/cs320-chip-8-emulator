(* ****** ****** *)
//
// HX-2017-02-09:
// Implementing Chip-8-Emulator
//
(* ****** ****** *)

#include "./../mydepies.hats"
#include "./../staloadall.hats"

staload STDLIB = "libats/libc/SATS/stdlib.sats"
staload UN = "prelude/SATS/unsafe.sats"

(* ****** ****** *)

#define CHIP_8_MAIN 1

(* ****** ****** *)

local #include "./chip-8-base.dats" in end
local #include "./chip-8-stack.dats" in end
local #include "./chip-8-mems.dats" in end
local #include "./chip-8-scr.dats" in end
local #include "./chip-8-regs.dats" in end
local #include "./chip-8-time.dats" in end
local #include "./chip-8-font.dats" in end
local #include "./chip-8-key.dats" in end
local #include "./chip-8-sdl.dats" in end
local #include "./chip-8-ops.dats" in end

(* ****** ****** *)

val interrupted = ref<bool>(false)
val max_cons_insns = ref<int>(0)
val frames_done = ref<int>(0)
val t_start = get_time()
val t_cur = ref<double>(get_time())

(* ****** ****** *)

implement quit() = !interrupted := true

(* ****** ****** *)

implement load_game(fname) = () where {
  exception GameNotFound of (string)
  exception GameTooLarge of (string)

  fun load(data: stream_vt(char), n: natLte(MEM_SIZE)):<cloref1> void =
    case+ !data of
    | ~stream_vt_nil() => ()
    | ~stream_vt_cons(c, rest) =>
      if n < MEM_SIZE then (Mem(n, c2b(c)); load(rest, n+1))
      else (stream_vt_free(rest); $raise GameTooLarge(fname))

  val game_opt = fileref_open_opt(fname, file_mode_r)
  val () = if option_vt_is_none(game_opt) then $raise GameNotFound(fname)
  val () = assertloc(option_vt_is_some(game_opt))
  val ~Some_vt(game) = game_opt
  val game_data = streamize_fileref_char(game)
  val () = load(game_data, PC_START)
}


(* ****** ****** *)

implement game_loop(dpy) = if not !interrupted then game_loop(dpy) where {
  fun exec_insns(i: int): void =
    if (i < !max_cons_insns) then (
      let
        val o = memory_get_word_exn()
        val () = exec_ins(o)
      in
        exec_insns(succ(i))
      end
    )

  val () = exec_insns(0)
  val () = poll_kb()
  val () = !t_cur := get_time()
  val elapsed = !t_cur - t_start
  val frames = g0float2int_double_int(elapsed * 60) - !frames_done
  val () = if frames > 0 then () where {
    val () = !frames_done := !frames_done + frames
    val () = DT.decr(i2b(frames))
    val () = DT.decr(i2b(frames))
    val () = update_display(dpy)
  }

  val () = !max_cons_insns := max(frames, 1) * INSNS_PER_FRAME
  val () = if (frames = 0) then $SDL2.SDL_Delay(1000 / 60)
}

(* ****** ****** *)

implement main0(argc, argv) = {
  val () = if argc != 2 then (
    if argc = 1 then fprintln!(stderr_ref, "chip8: No game specified")
    else fprintln!(stderr_ref, "chip8: Unknown arguments");
    exit(1)
  )
  val () = assertloc(argc = 2)
  val () = load_font()
  val () = load_game(argv[1])
  val dpy = init_display()
  val () = PC.set(PC_START)
  val () = game_loop(dpy)
  val () = close_display(dpy)
}

(* end of [chip-8-main.dats] *)
