(* ****** ****** *)
//
// RK-2017-04-21
// Implementing timing mechanisms
//
(* ****** ****** *)

#ifndef CHIP_8_MAIN
#include "./../staloadall.hats"
#endif

(* ****** ****** *)

staload UN = "prelude/SATS/unsafe.sats"
staload TIME = "libats/libc/SATS/sys/time.sats"

(* ****** ****** *)

local
  assume timer_type = ref(byte)
in
  implement timer_get(t) = !t
  implement timer_set(t, v) = !t := v
  implement timer_decr(t, amt) =
    if amt > t.get() then t.set(b_0x0) else t.set(t.get() - amt)
end

(* ****** ****** *)

extern castfn ref2timer : ref(byte) -> timer

(* ****** ****** *)

implement DT = ref2timer(ref<byte>(b_0x0))
implement ST = ref2timer(ref<byte>(b_0x0))

(* ****** ****** *)

implement get_time() = t where {
  var tv: $TIME.timeval?
  val err = $TIME.gettimeofday(tv)
  val () = assertloc(err >= 0)
  prval () = opt_unsome(tv)
  val t = $UN.cast{double}(tv.tv_sec) + $UN.cast{double}(tv.tv_usec) / 1000000
}

(* End of [chip-8-key.dats] *)
