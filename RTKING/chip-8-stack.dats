(* ****** ****** *)
//
// RK-2017-02-13
// Implementing stack
//
(* ****** ****** *)

#include "./../staloadall.hats"

(* ****** ****** *)

typedef istk = natLte(STK_SIZE)

(* ****** ****** *)

local
  val stk_ptr = ref<istk>(0)
  val stksz = i2sz(STK_SIZE)
  val mystk = arrayref_make_elt<imem>(stksz, 0)
  exception StackUnderflow of ()
  exception StackOverflow of ()
in
  implement stack_pop() =
    let val i = !stk_ptr in
      if i > 0 then (!stk_ptr := i-1; mystk[i-1])
      else $raise StackUnderflow()
    end

  implement stack_push(a) =
    let val i = !stk_ptr in
      if i < STK_SIZE then (mystk[i] := a; !stk_ptr := i+1)
      else $raise StackOverflow()
    end
end

(* End of [chip-8-stack.dats] *)