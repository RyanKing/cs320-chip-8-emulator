(* ****** ****** *)
//
// RK-2017-02-13:
// Basic Functionality
//
(* ****** ****** *)

#include "./../staloadall.hats"

(* ****** ****** *)

implement print_byte(x) = fprint(stdout_ref, x)
implement print_word(x) = fprint(stdout_ref, x)

(* ****** ****** *)

implement fprint_byte(out, x) = $extfcall(void, "fprintf", out, "B(%02x)", x)
implement fprint_word(out, x) = $extfcall(void, "fprintf", out, "W(%04x)", x)

(* End of [chip-8-base-dats] *)
