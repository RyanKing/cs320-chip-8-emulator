(* ****** ****** *)
//
// RK-2017-02-13
// Implementing registers
//
(* ****** ****** *)

#include "./../staloadall.hats"

staload UN = "prelude/SATS/unsafe.sats"

(* ****** ****** *)

local
  assume breg_type = ref(byte)
  assume areg_type = ref(imem)
in
  implement breg_get(r) = !r
  implement breg_set(r, v) = !r := v

  implement areg_get(r) = !r
  implement areg_set(r, v) = !r := i2a(v)

  implement areg_incr(r, amt) = areg_set(r, !r + amt)
end

(* ****** ****** *)

extern castfn ref2breg : ref(byte) -> breg
extern castfn ref2areg : ref(imem) -> areg

(* ****** ****** *)

implement I = ref2areg(ref<imem>(0))
implement PC = ref2areg(ref<imem>(0))

(* ****** ****** *)

local
  val rsz = i2sz(NREG)
  val bregs = arrayref_make_elt<breg?>(rsz, $extval(breg?, "0"))
  val bregs = $UNSAFE.cast{arrayref(breg, 16)}(bregs)
  val _ = arrayref_foreach(bregs, rsz) where {
    implement array_foreach$fwork<breg><void>(x, env) =
      (x := ref2breg(ref<byte>(b_0x0)))
  }
in
  implement V(i) = bregs[i]
end

(* End of [chip-8-regs.dats] *)