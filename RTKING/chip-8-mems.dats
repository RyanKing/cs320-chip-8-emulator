(* ****** ****** *)
//
// RK-2017-02-13
// Implementing memory
//
(* ****** ****** *)

#ifndef CHIP_8_MAIAN
#include "./../staloadall.hats"
#endif

(* ****** ****** *)

#staload UN = "prelude/SATS/unsafe.sats"

(* ****** ****** *)

local
  val memsz = i2sz(MEM_SIZE)
  val bmems = arrayref_make_elt<byte>(memsz, b_0x0)
in
  implement memory_get_at(i) = bmems[i]
  implement memory_set_at(i, v) = bmems[i] := v
end

(* ****** ****** *)

implement chip8_imem_of_int(a) = $UN.cast{natLt(MEM_SIZE)}(a) where {
  exception MemOutOfBounds of (int)
  val () = if a < 0 || a >= MEM_SIZE then $raise MemOutOfBounds(a)
}

(* ****** ****** *)

implement memory_get_byte_exn() = Mem(PC.get())

(* ****** ****** *)

implement memory_get_word_exn() = word_make_byte_byte(Mem(i0), Mem(i1)) where {
  val i0 = PC.get()
  val i0 = $UN.cast{intGte(0)}(i0)
  val i1 = succ(i0)
  val () = assertloc(i1 < MEM_SIZE)
}

(* End of [chip-8-mems.dats] *)