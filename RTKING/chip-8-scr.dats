(* ****** ****** *)
//
// RK-2017-02-13
// Implementing screen
//
(* ****** ****** *)

#include "./../staloadall.hats"

(* ****** ****** *)

local
  val scrwsz = i2sz(SCR_WIDTH)
  val scrhsz = i2sz(SCR_HEIGHT)
  val screen = matrixref_make_elt<byte>(scrwsz, scrhsz, b_0x0)
in
  implement screen_get_at(px, py) = screen[px, SCR_HEIGHT, py]

  implement screen_set_at(px, py, v) = screen[px, SCR_HEIGHT, py] := v

  implement screen_clear() = matrixref_foreach(screen, scrwsz, scrhsz) where {
    implement matrix_foreach$fwork<byte><void>(px, env) = px := b_0x0
  }
end

(* End of [chip-8-scr.dats] *)