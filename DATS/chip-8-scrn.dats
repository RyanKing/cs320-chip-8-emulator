(* ****** ****** *)
//
// RK-2017-02-13
// Implementing screen
//
(* ****** ****** *)

#ifdef
CHIP_8_MAIN
#then
#else
//
#include "./../staloadall.hats"
//
#endif // end of [#ifdef]

(* ****** ****** *)

local
//
val scrwsz = i2sz(SCR_WIDTH)
val scrhsz = i2sz(SCR_HEIGHT)
//
val screen =
matrixref_make_elt<byte>(scrwsz, scrhsz, b_0x0)
//
in

implement
screen_get_at(px, py) =
screen[px, SCR_HEIGHT, py]
implement
screen_set_at(px, py, v) =
screen[px, SCR_HEIGHT, py] := v

implement
screen_clear() =
matrixref_foreach
(
screen, scrwsz, scrhsz
) where
{
implement
matrix_foreach$fwork<byte><void>(px, env) = px := b_0x0
} (* end of [screen_clear] *)

end // end of [local]
          
(* ****** ****** *)

(* end of [chip-8-scrn.dats] *)
          