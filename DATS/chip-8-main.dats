(* ****** ****** *)
//
// HX-2017-02-09:
// Chip-8-Emulator
//
(* ****** ****** *)
//
(*
#include "./../mydepies.hats"
*)
//
(* ****** ****** *)
//
#include "./../staloadall.hats"
//
(* ****** ****** *)

#define CHIP_8_MAIN 1

(* ****** ****** *)
//
local
#include "./chip-8-base.dats"
in (* nothing *) end // local
//
(* ****** ****** *)
//
local
#include "./chip-8-regs.dats"
in (* nothing *) end // local
//
(* ****** ****** *)
//
local
#include "./chip-8-stck.dats"
in (* nothing *) end // local
//
(* ****** ****** *)
//
local
#include "./chip-8-memy.dats"
in (* nothing *) end // local
//
(* ****** ****** *)
//
local
#include "./chip-8-opcd.dats"
in (* nothing *) end // local
//
(* ****** ****** *)
//
local
#include "./chip-8-scrn.dats"
in (* nothing *) end // local
//
(* ****** ****** *)
//
local
#include "./chip-8-font.dats"
in (* nothing *) end // local
//
(* ****** ****** *)
//
(*
local
#include "./chip-8-sdl2.dats"
in (* nothing *) end // local
*)
//
(* ****** ****** *)

implement
main0() =
{
//
val () =
println!
(
  "Hello from [chip-8-main]!"
) (* println! *)
//
val () = println! ("b_0x1 = ", b_0x1)
val () = println! ("w_0x1 = ", w_0x1)
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [chip-8-main.dats] *)
