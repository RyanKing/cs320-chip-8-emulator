(* ****** ****** *)
//
// HX-2017-02-13:
// Implementing stack
//
(* ****** ****** *)
//
#ifdef
CHIP_8_MAIN
#then
#else
//
#include "./../staloadall.hats"
//
#endif // end of [#ifdef]
//
(* ****** ****** *)

typedef
istk = intBtwe(0, STK_SIZE)

(* ****** ****** *)

local
//
val
myidx = ref<istk>(0)
val
mystk =
arrayref_make_elt<word>
  (i2sz(STK_SIZE), w_0x0)
//
in

implement
stack_pop() = let
  val i = !myidx
in
//
if (
i > 0
) then (
  !myidx := i-1; mystk[i-1]
) else $raise StackUnderflowExn()
end // end of [stack_pop]

implement
stack_push(x) = let
  val i = !myidx
in
//
if (
i < STK_SIZE
) then (
  mystk[i] := x; !myidx := i+1
) else $raise StackOverflowExn()
//
end // end of [stack_push]

end // end of [local]

(* ****** ****** *)

(* end of [chip-8-stck.dats] *)

