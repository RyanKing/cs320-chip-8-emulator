(* ****** ****** *)
//
// HX-2017-02-09:
// Some basic stuff
//
(* ****** ****** *)
//
#ifdef
CHIP_8_MAIN
#then
#else
//
#include "./../staloadall.hats"
//
#endif // end of [#ifdef]
//
(* ****** ****** *)
//
implement
print_byte(x) = fprint(stdout_ref, x)
implement
print_word(x) = fprint(stdout_ref, x)
//
(* ****** ****** *)
//
implement
fprint_byte(out, x) =
  $extfcall(void, "fprintf", out, "B(%02x)", x)
implement
fprint_word(out, x) =
  $extfcall(void, "fprintf", out, "W(%04x)", x)
//
(* ****** ****** *)

(* end of [chip-8-base.dats] *)
