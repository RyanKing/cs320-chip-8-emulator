(* ****** ****** *)
//
// RK-2017-02-13
// SDL2-related implementation
//
(* ****** ****** *)
//
#include "./../mydepies.hats"
//
#staload $SDL2 // opening the namespace
//
#staload "./../SATS/SDL2_extra.sats"
//
(* ****** ****** *)
//
fun{}
game_quit(): void =
let
//
val () = SDL_Quit() in exit(0)
//
end // end of [game_quit]
//
(* ****** ****** *)

fun{}
handle_key
(
  event: SDL_Event
) : Option(int) = let
//
val sym = GetKeysym(event)
//
fun sgn(x: int) =
(
if event.type = SDL_KEYUP then x else ~x
) : int // end of [fun]
in
//
ifcase
| sym = SDLK_1 => Some(sgn(0x1))
| sym = SDLK_2 => Some(sgn(0x2))
| sym = SDLK_3 => Some(sgn(0x3))
| sym = SDLK_4 => Some(sgn(0xF))
| sym = SDLK_q => Some(sgn(0x4))
| sym = SDLK_w => Some(sgn(0x5))
| sym = SDLK_e => Some(sgn(0x6))
| sym = SDLK_r => Some(sgn(0xE))
| sym = SDLK_a => Some(sgn(0x7))
| sym = SDLK_s => Some(sgn(0x8))
| sym = SDLK_d => Some(sgn(0x9))
| sym = SDLK_f => Some(sgn(0xD))
| sym = SDLK_z => Some(sgn(0xA))
| sym = SDLK_x => Some(sgn(0x0))
| sym = SDLK_c => Some(sgn(0xB))
| sym = SDLK_v => Some(sgn(0xC))
| _(*all-of-the-other-keys*) => None()
//
end // end of [handle_key]

(* ****** ****** *)

(* end of [chip-8-sdl2.dats] *)
