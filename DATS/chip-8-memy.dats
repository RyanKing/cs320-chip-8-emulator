(* ****** ****** *)
//
// Forcing external bounds-checking
//
(* ****** ****** *)

#ifdef
CHIP_8_MAIN
#then
#else
//
#include "./../staloadall.hats"
//
#endif // end of [#ifdef]

(* ****** ****** *)

#staload UN = "prelude/SATS/unsafe.sats"

(* ****** ****** *)

local
//
val
bmems =
arrayref_make_elt<byte>
  (i2sz(MEM_SIZE), b_0x0)
//
in (* in-of-local *)

implement memory_get_at(i) = bmems[i]
implement memory_set_at(i, v) = bmems[i] := v

end // end of [local]

(* ****** ****** *)

implement
memory_get_byte_exn
  () = let
//
val i0 = PC.get()
val i0 = $UN.cast{intGte(0)}(i0)
val () = assertloc(i0 < MEM_SIZE)
//
in
  Mem(i0)
end // end of [memory_get_byte_exn]

(* ****** ****** *)

implement
memory_get_word_exn
  () = let
//
val i0 = PC.get()
val i0 = $UN.cast{intGte(0)}(i0)
val i1 = succ( i0 )
val () = assertloc(i1 < MEM_SIZE)
//
in
  word_make_byte_byte(Mem(i0), Mem(i1))
end // end of [memory_get_byte_exn]

(* ****** ****** *)

(* end of [chip-8-memy.dats] *)
