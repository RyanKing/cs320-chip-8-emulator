(* ****** ****** *)
//
// HX-2017-02-09:
// Implementing registers
//
(* ****** ****** *)
//
#ifdef
CHIP_8_MAIN
#then
#else
//
#include "./../staloadall.hats"
//
#endif // end of [#ifdef]
//
(* ****** ****** *)

local
//
assume breg_type = ref(byte)
assume wreg_type = ref(word)
//
in
//
implement breg_get(r) = !r
implement breg_set(r, v) = !r := v
//
implement wreg_get(r) = !r
implement wreg_set(r, v) = !r := v
//
end // end of [local]

(* ****** ****** *)
//
extern
castfn
ref2breg : ref(byte) -> breg
extern
castfn
ref2wreg : ref(word) -> wreg
//
(* ****** ****** *)
//
implement
I(*val*) =
ref2wreg(ref<word>(w_0x0))
implement
PC(*val*) =
ref2wreg(ref<word>(w_0x0))
//
(* ****** ****** *)

local
//
val
asz = i2sz(16)
//
val
bregs =
arrayref_make_elt<breg?>
  (asz, $extval(breg?, "0"))
//
val
bregs =
$UNSAFE.cast
{arrayref(breg,16)}(bregs)
//
val _(*asz*) =
arrayref_foreach(bregs, asz) where
{
//
implement
array_foreach$fwork<breg><void>
  (x, env) =
  (x := ref2breg(ref<byte>(b_0x0)))
//
}
//
in (* in-of-local *)

implement V(i) = bregs[i]

end // end of [local]

(* ****** ****** *)

(* end of [chip-8-regs.dats] *)
