(* ****** ****** *)
//
// RK-2017-02-26
// Implementing opcodes
//
(* ****** ****** *)

#ifdef
CHIP_8_MAIN
#then
#else
//
#include "./../staloadall.hats"
//
#endif // end of [#ifdef]

(* ****** ****** *)
//
// For chip8 opcodes
//
(* ****** ****** *)

datatype
opcode =
| OP0NNN of (word)
| OP00E0 of ((*void*))
| OP00EE of ((*void*))
| OP1NNN of (word)
| OP2NNN of (word)
| OP3XNN of (breg, byte)
| OP4XNN of (breg, byte)
| OP5XY0 of (breg, breg)
| OP6XNN of (breg, byte)
| OP7XNN of (breg, byte)
| OP8XY0 of (breg, breg)
| OP8XY1 of (breg, breg)
| OP8XY2 of (breg, breg)
| OP8XY3 of (breg, breg)
| OP8XY4 of (breg, breg)
| OP8XY5 of (breg, breg)
| OP8XY6 of (breg, breg)
| OP8XY7 of (breg, breg)
| OP8XYE of (breg, breg)
| OP9XY0 of (breg, breg)
| OPANNN of (word)
| OPBNNN of (word)
| OPCXNN of (breg, uint)
| OPDXYN of (breg, breg, byte)
| OPEX9E of (breg)
| OPEXA1 of (breg)
| OPFX07 of (breg)
| OPFX0A of (breg)
| OPFX15 of (breg)
| OPFX18 of (breg)
| OPFX1E of (breg)
| OPFX29 of (breg)
| OPFX33 of (breg)
| OPFX55 of (breg)
| OPFX65 of (breg)

(* ****** ****** *)


(* ****** ****** *)

(* end of [chip08_opcd.dats] *)
