(* ****** ****** *)
//
// HX-2017-02-09:
// Chip-8-Emulator
//
(* ****** ****** *)
//
#include "share/atspre_staload.hats"
#include "share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)
//
staload "./SATS/chip-8-main.sats"
staload SDL2e = "./SATS/SDL2_extra.sats"
//
(* ****** ****** *)

(* end of [staloadall.hats] *)
